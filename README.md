# 1of0/iptmerge

[![pipeline status](https://gitlab.com/1of0/csharp/iptmerge/badges/master/pipeline.svg)](https://gitlab.com/1of0/csharp/iptmerge/-/commits/master)
[![coverage report](https://gitlab.com/1of0/csharp/iptmerge/badges/master/coverage.svg)](https://gitlab.com/1of0/csharp/iptmerge/-/commits/master)

## What

A tool to merge two `iptables-restore` compatible dumps.

## Dependencies

- [CommandLineParser](https://github.com/commandlineparser/commandline)
- [Json.NET](https://github.com/JamesNK/Newtonsoft.Json)

### Dev dependencies

- [xUnit.net](https://github.com/xunit/xunit)
- [Coverlet](https://github.com/coverlet-coverage/coverlet)

## License

This project is licensed under the MIT license. See [LICENSE.md](LICENSE.md).
