using System.Text;
using OneOfZero.Iptmerge.Util;

//
// SPDX-License-Identifier: MIT
// Find the full license text at: https://gitlab.com/1of0/csharp/iptmerge/-/blob/master/LICENSE.md
//

namespace OneOfZero.Iptmerge.Tests.Fixture
{
    public class ConsoleFixture : IConsole
    {
        public StringBuilder Input { get; } = new StringBuilder();

        public StringBuilder Output { get; } = new StringBuilder();

        public string ReadStandardInputAsString()
        {
            return Input.ToString();
        }

        public void WriteStandardOutput(string buffer)
        {
            Output.Append(buffer);
        }
    }
}
