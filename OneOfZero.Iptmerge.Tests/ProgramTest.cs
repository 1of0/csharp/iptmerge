using System.IO;
using OneOfZero.Iptmerge.Exceptions;
using OneOfZero.Iptmerge.Tests.Fixture;
using Xunit;

//
// SPDX-License-Identifier: MIT
// Find the full license text at: https://gitlab.com/1of0/csharp/iptmerge/-/blob/master/LICENSE.md
//

namespace OneOfZero.Iptmerge.Tests
{
    public class ProgramTest
    {
        [Fact]
        public void TestArguments()
        {
            var consoleFixture = new ConsoleFixture();
            Program.Console = consoleFixture;

            Program.Main(new[]
            {
                "Fixture/merge-behaviour.json",
                "Fixture/generated.state",
                "Fixture/running.state",
            });

            Assert.Equal(
                File.ReadAllText("Fixture/merged.whitelist.whitelist.state"),
                consoleFixture.Output.ToString()
            );
        }

        [Fact]
        public void TestStandardInput()
        {
            var consoleFixture = new ConsoleFixture();
            consoleFixture.Input.Append(File.ReadAllText("Fixture/running.state"));

            Program.Console = consoleFixture;

            Program.Main(new[]
            {
                "Fixture/merge-behaviour.json",
                "Fixture/generated.state",
            });

            Assert.Equal(
                File.ReadAllText("Fixture/merged.whitelist.whitelist.state"),
                consoleFixture.Output.ToString()
            );
        }

        [Fact]
        public void TestMissingBehaviourConfigFile()
        {
            var consoleFixture = new ConsoleFixture();

            Program.Console = consoleFixture;

            Assert.Throws<ParseException>(() =>
            {
                Program.Main(new[]
                {
                    "Fixture/non-existent.json",
                    "Fixture/generated.state",
                    "Fixture/running.state",
                });
            });
        }
    }
}
