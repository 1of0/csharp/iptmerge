using System;
using OneOfZero.Iptmerge.Exceptions;
using OneOfZero.Iptmerge.Netfilter.Parse;
using OneOfZero.Iptmerge.Netfilter.Structure;
using Xunit;

//
// SPDX-License-Identifier: MIT
// Find the full license text at: https://gitlab.com/1of0/csharp/iptmerge/-/blob/master/LICENSE.md
//

namespace OneOfZero.Iptmerge.Tests.Netfilter.Parse
{
    public class StateParserTest
    {
        [Theory]
        [MemberData(nameof(ValidateParseStateData))]
        public void TestValidParseState(string stateDefinition, State expectedState)
        {
            var parser = new StateParser();
            Assert.Equal(expectedState, parser.Parse(stateDefinition));
        }

        [Theory]
        [MemberData(nameof(InvalidParseStateData))]
        public void TestInvalidParseState(string stateDefinition, Type expectedException)
        {
            var parser = new StateParser();
            Assert.Throws(expectedException, () => parser.Parse(stateDefinition));
        }

        public static TheoryData<string, State> ValidateParseStateData => new TheoryData<string, State>
        {
            {
                "#comment\n*foo\nCOMMIT\n\n*bar\nCOMMIT\n",
                new State(
                    new[]
                    {
                        new Table("foo"),
                        new Table("bar"),
                    }
                )
            },
            {
                "#comment\n*foo\n:INPUT ACCEPT [0:0]\nCOMMIT\n",
                new State(
                    new[]
                    {
                        new Table("foo", new[] {new Chain("INPUT", "ACCEPT", "[0:0]")}, new Rule[] { }),
                    }
                )
            },
            {
                "#comment\n*foo\n:INPUT ACCEPT [0:0]\n-A INPUT -s 127.0.0.1 -j ACCEPT\nCOMMIT\n",
                new State(
                    new[]
                    {
                        new Table(
                            "foo",
                            new[] {new Chain("INPUT", "ACCEPT", "[0:0]")},
                            new[]
                            {
                                new Rule("A", "INPUT",
                                    new[] {new RuleOption("s", "127.0.0.1"), new RuleOption("j", "ACCEPT")}),
                            }
                        ),
                    }
                )
            },
            {
                "",
                new State(new Table[] { })
            },
            {
                "#comment",
                new State(new Table[] { })
            },
        };

        public static TheoryData<string, Type> InvalidParseStateData = new TheoryData<string, Type>
        {
            {":INPUT\n*foo\nCOMMIT\n", typeof(InvalidDefinitionException)},
        };
    }
}
