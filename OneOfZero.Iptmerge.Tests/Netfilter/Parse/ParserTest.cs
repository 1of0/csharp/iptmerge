﻿using System;
using System.Collections.Generic;
using System.Linq;
using OneOfZero.Iptmerge.Exceptions;
using OneOfZero.Iptmerge.Netfilter.Parse;
using OneOfZero.Iptmerge.Netfilter.Structure;
using Xunit;

//
// SPDX-License-Identifier: MIT
// Find the full license text at: https://gitlab.com/1of0/csharp/iptmerge/-/blob/master/LICENSE.md
//

namespace OneOfZero.Iptmerge.Tests.Netfilter.Parse
{
    using ValidDefinitionsTheoryData = TheoryData<string, IEnumerable<Rule>>;

    public class ParserTest
    {
        [Theory]
        [MemberData(nameof(ValidDefinitionsData))]
        public void TestParseRuleValidInput(string definition, IEnumerable<Rule> expectedRules)
        {
            var parser = new RuleParser();

            var actualRules = parser.Parse(definition).ToList();
            Assert.Equal(expectedRules, actualRules);

            if (actualRules.Count == 1)
            {
                var escapableCharacters = ParseHelper.EscapeMap.Keys;

                var fuzzyExpected = escapableCharacters.Aggregate(
                    definition,
                    (currentString, escapableCharacter) => currentString.Replace(escapableCharacter, "")
                );
                var fuzzyActual = escapableCharacters.Aggregate(
                    actualRules.First().ToString(),
                    (currentString, escapableCharacter) => currentString.Replace(escapableCharacter, "")
                );

                Assert.Equal(fuzzyExpected, fuzzyActual);
            }
        }

        [Theory]
        [MemberData(nameof(InvalidDefinitionsData))]
        public void TestParseRuleInvalidInput(string definition, Type exceptionType)
        {
            Assert.Throws(exceptionType, () =>
            {
                var parser = new RuleParser();
                parser.Parse(definition);
            });
        }

        public static ValidDefinitionsTheoryData ValidDefinitionsData() => new ValidDefinitionsTheoryData
        {
            {
                "-A INPUT -p tcp --dport 1234 -j ACCEPT",
                new [] {
                    new Rule(
                        "A",
                        "INPUT",
                        new [] {
                            new RuleOption("p", new [] {"tcp"}),
                            new RuleOption("dport", new [] {"1234"}),
                            new RuleOption("j", new [] {"ACCEPT"}),
                        }
                    ),
                }
            },
            {
                "-I INPUT -p tcp --dport 1234 -j ACCEPT",
                new [] {
                    new Rule(
                        "I",
                        "INPUT",
                        new [] {
                            new RuleOption("p", new [] {"tcp"}),
                            new RuleOption("dport", new [] {"1234"}),
                            new RuleOption("j", new [] {"ACCEPT"}),
                        }
                    ),
                }
            },
            {
                "-I INPUT -p tcp ! --dport 1234 -j ACCEPT",
                new [] {
                    new Rule(
                        "I",
                        "INPUT",
                        new [] {
                            new RuleOption("p", new [] {"tcp"}),
                            new RuleOption("dport", new [] {"1234"}, true),
                            new RuleOption("j", new [] {"ACCEPT"}),
                        }
                    ),
                }
            },
            {
                "-I INPUT -m multiport --dports 123,456,789 -m comment --comment \"foo\" -j ACCEPT",
                new [] {
                    new Rule(
                        "I",
                        "INPUT",
                        new [] {
                            new RuleOption("m", new [] {"multiport"}),
                            new RuleOption("dports", new [] {"123,456,789"}),
                            new RuleOption("m", new [] {"comment"}),
                            new RuleOption("comment", new [] {"foo"}),
                            new RuleOption("j", new [] {"ACCEPT"}),
                        }
                    ),
                }
            },
            {
                "-X CUSTOM-CHAIN",
                new [] {new Rule("X", "CUSTOM-CHAIN", new RuleOption[] {})}
            },
            {
                "-A INPUT -s 1.1.1.1/32,2.2.2.2/32,3.3.3.3/32 -j ACCEPT",
                new [] {
                    new Rule("A", "INPUT", new [] {new RuleOption("s", new [] {"1.1.1.1/32"}), new RuleOption("j", new [] {"ACCEPT"})}),
                    new Rule("A", "INPUT", new [] {new RuleOption("s", new [] {"2.2.2.2/32"}), new RuleOption("j", new [] {"ACCEPT"})}),
                    new Rule("A", "INPUT", new [] {new RuleOption("s", new [] {"3.3.3.3/32"}), new RuleOption("j", new [] {"ACCEPT"})}),
                }
            },
            {
                "-A INPUT -s 1.1.1.1/32 -j ACCEPT",
                new [] {
                    new Rule("A", "INPUT", new [] {new RuleOption("s", new [] {"1.1.1.1/32"}), new RuleOption("j", new [] {"ACCEPT"})}),
                }
            },
        };

        public static TheoryData<string, Type> InvalidDefinitionsData => new TheoryData<string, Type>
        {
            {"foo -A INPUT -j ACCEPT", typeof(UnexpectedTokenException)},
            {"-p tcp --dport 1234 -j ACCEPT", typeof(InvalidActionException)},
            {"-R INPUT 1 -j ACCEPT", typeof(InvalidActionException)},
            {"-A -j ACCEPT", typeof(InvalidDefinitionException)},
            {"", typeof(InvalidActionException)},
            {"-A INPUT -s foo -s bar", typeof(InvalidDefinitionException)},
            {"-A INPUT -s foo bar", typeof(InvalidDefinitionException)},
            {"-A INPUT -A INPUT", typeof(InvalidActionException)},
        };
    }
}
