using OneOfZero.Iptmerge.Netfilter.Parse;
using Xunit;

//
// SPDX-License-Identifier: MIT
// Find the full license text at: https://gitlab.com/1of0/csharp/iptmerge/-/blob/master/LICENSE.md
//

namespace OneOfZero.Iptmerge.Tests.Netfilter.Parse
{
    public class ParseHelperTest
    {
        [Theory]
        [MemberData(nameof(UnescapedValueData))]
        public void TestEscapeUnescape(string value)
        {
            Assert.Equal(value, value.ShellEscape().ShellUnescape());
        }

        [Theory]
        [MemberData(nameof(EscapedValueData))]
        public void TestUnescapeEscape(string value, string? expectedOutput = null)
        {
            Assert.Equal(expectedOutput ?? value, value.ShellUnescape().ShellEscape());
        }

        [Theory]
        [MemberData(nameof(CanonicalizedData))]
        public void TestCanonicalization(string value, string expectedCanonicalValue)
        {
            Assert.Equal(expectedCanonicalValue, value.Canonicalize());
        }

        public static TheoryData<string> UnescapedValueData = new TheoryData<string>
        {
            "Foo bar baz",
            "Foo \"bar\" baz",
            "Foo 'bar' baz",
            "Foo \\bar baz",
            "Foo bar baz\\\\",
            "Foo bar baz\\",
            "Foo bar baz\\\'",
            "Foo bar baz\\\\\'",
            "\"Foo bar baz\"",
            "'Foo bar baz'",
        };

        public static TheoryData<string, string?> EscapedValueData = new TheoryData<string, string?>
        {
            {"'Foo bar baz'", null},
            {"'Foo \\'bar\\' baz'", null},
            {"'Foo \\\"bar\\\" baz'", null},
            {"'Foo\\\\bar\\\\baz'", null},
            {"\"Foo bar baz\"", "'Foo bar baz'"},
            {"Foo bar baz", "'Foo bar baz'"},
        };

        public static TheoryData<string, string> CanonicalizedData = new TheoryData<string, string>
        {
            {"127.0.0.1", "127.0.0.1/32"},
            {"127.0.0.1/32", "127.0.0.1/32"},
            {"127.0.0.1/24", "127.0.0.1/24"},
            {"127.1", "127.0.0.1/32"},
            {"::1", "::1/128"},
            {":", ":"},
            {".", "."},
            {"foo", "foo"},
            {"foo/1234", "foo/1234"},
            {"/1234", "/1234"},
            {"//", "//"},
            {"foo/bar", "foo/bar"},
            {"127.1/1234", "127.1/1234"},
        };
    }
}
