using System;
using System.Collections.Generic;
using OneOfZero.Iptmerge.Exceptions;
using OneOfZero.Iptmerge.Netfilter.Parse;
using Xunit;

//
// SPDX-License-Identifier: MIT
// Find the full license text at: https://gitlab.com/1of0/csharp/iptmerge/-/blob/master/LICENSE.md
//

namespace OneOfZero.Iptmerge.Tests.Netfilter.Parse
{
    using ValidDefinitionsTheoryData = TheoryData<string, IEnumerable<string>, IEnumerable<IEnumerable<string>>>;

    public class RuleTokenizerTest
    {
        [Theory]
        [MemberData(nameof(ValidDefinitionsData))]
        public void TestTokenizerValidInput(
            string definition,
            IEnumerable<string> expectedTokens,
            IEnumerable<IEnumerable<string>> expectedTokenGroups
        )
        {
            var actualTokens = definition.Tokenize();
            Assert.Equal(expectedTokens, actualTokens);

            var actualTokenGroups = actualTokens.GroupTokens();
            Assert.Equal(expectedTokenGroups, actualTokenGroups);
        }

        [Theory]
        [MemberData(nameof(InvalidDefinitionsData))]
        public void TestTokenizerInvalidInput(string definition, Type exceptionType)
        {
            Assert.Throws(exceptionType, definition.Tokenize);
        }

        public static ValidDefinitionsTheoryData ValidDefinitionsData => new ValidDefinitionsTheoryData
        {
            {
                "-A INPUT -p tcp --dport 1234 -j ACCEPT",
                new[] {"-A", "INPUT", "-p", "tcp", "--dport", "1234", "-j", "ACCEPT"},
                new[]
                {
                    new[] {"-A", "INPUT"}, new[] {"-p", "tcp"}, new[] {"--dport", "1234"}, new[] {"-j", "ACCEPT"},
                }
            },
            {
                "-A INPUT -p\ntcp        --dport 1234    -j\tACCEPT         ",
                new[] {"-A", "INPUT", "-p", "tcp", "--dport", "1234", "-j", "ACCEPT"},
                new[]
                {
                    new[] {"-A", "INPUT"}, new[] {"-p", "tcp"}, new[] {"--dport", "1234"}, new[] {"-j", "ACCEPT"},
                }
            },
            {
                "-A INPUT -p=tcp --dport 1234 -j ACCEPT",
                new[] {"-A", "INPUT", "-p", "=tcp", "--dport", "1234", "-j", "ACCEPT"},
                new[]
                {
                    new[] {"-A", "INPUT"}, new[] {"-p", "=tcp"}, new[] {"--dport", "1234"}, new[] {"-j", "ACCEPT"},
                }
            },
            {
                "-A INPUT -p tcp --dport=1234 -j ACCEPT",
                new[] {"-A", "INPUT", "-p", "tcp", "--dport", "1234", "-j", "ACCEPT"},
                new[]
                {
                    new[] {"-A", "INPUT"}, new[] {"-p", "tcp"}, new[] {"--dport", "1234"}, new[] {"-j", "ACCEPT"},
                }
            },
            {
                "-A INPUT -p tcp ! --dport 1234 -j ACCEPT",
                new[] {"-A", "INPUT", "-p", "tcp", "!", "--dport", "1234", "-j", "ACCEPT"},
                new[]
                {
                    new[] {"-A", "INPUT"}, new[] {"-p", "tcp"}, new[] {"!", "--dport", "1234"},
                    new[] {"-j", "ACCEPT"},
                }
            },
            {
                "-AINPUT -p tcp ! --dport 1234 -j ACCEPT",
                new[] {"-A", "INPUT", "-p", "tcp", "!", "--dport", "1234", "-j", "ACCEPT"},
                new[]
                {
                    new[] {"-A", "INPUT"}, new[] {"-p", "tcp"}, new[] {"!", "--dport", "1234"},
                    new[] {"-j", "ACCEPT"},
                }
            },
            {
                "-A INPUT -p\"tcp\" --dport 1234 -j ACCEPT",
                new[] {"-A", "INPUT", "-p", "tcp", "--dport", "1234", "-j", "ACCEPT"},
                new[]
                {
                    new[] {"-A", "INPUT"}, new[] {"-p", "tcp"}, new[] {"--dport", "1234"}, new[] {"-j", "ACCEPT"},
                }
            },
            {
                "-A INPUT -p tcp --dport \"1234\" -j ACCEPT",
                new[] {"-A", "INPUT", "-p", "tcp", "--dport", "1234", "-j", "ACCEPT"},
                new[]
                {
                    new[] {"-A", "INPUT"}, new[] {"-p", "tcp"}, new[] {"--dport", "1234"}, new[] {"-j", "ACCEPT"},
                }
            },
            {
                "-A INPUT -p tcp --dport=\"1234\" -j ACCEPT",
                new[] {"-A", "INPUT", "-p", "tcp", "--dport", "1234", "-j", "ACCEPT"},
                new[]
                {
                    new[] {"-A", "INPUT"}, new[] {"-p", "tcp"}, new[] {"--dport", "1234"}, new[] {"-j", "ACCEPT"},
                }
            },
            {
                "-A INPUT -ptcp --dport 1234 -j ACCEPT",
                new[] {"-A", "INPUT", "-p", "tcp", "--dport", "1234", "-j", "ACCEPT"},
                new[]
                {
                    new[] {"-A", "INPUT"}, new[] {"-p", "tcp"}, new[] {"--dport", "1234"}, new[] {"-j", "ACCEPT"},
                }
            },
            {
                "-A INPUT \"-p\" \"tcp\" \"--dport\" \"1234\" \"-j\" ACCEPT",
                new[] {"-A", "INPUT", "-p", "tcp", "--dport", "1234", "-j", "ACCEPT"},
                new[]
                {
                    new[] {"-A", "INPUT"}, new[] {"-p", "tcp"}, new[] {"--dport", "1234"}, new[] {"-j", "ACCEPT"},
                }
            },
            {
                "-A INPUT \"-p tcp\" --dport 1234 -j ACCEPT",
                new[] {"-A", "INPUT", "-p tcp", "--dport", "1234", "-j", "ACCEPT"},
                new[] {new[] {"-A", "INPUT"}, new[] {"-p tcp"}, new[] {"--dport", "1234"}, new[] {"-j", "ACCEPT"}}
            },
            {
                "-A INPUT \"-p tcp\" \"--dport=1234\" -j ACCEPT",
                new[] {"-A", "INPUT", "-p tcp", "--dport=1234", "-j", "ACCEPT"},
                new[] {new[] {"-A", "INPUT"}, new[] {"-p tcp"}, new[] {"--dport=1234"}, new[] {"-j", "ACCEPT"}}
            },
            {
                "-A INPUT -j ACCEPT -m comment --comment \"Foo \\\"Bar\\\" Baz\"",
                new[] {"-A", "INPUT", "-j", "ACCEPT", "-m", "comment", "--comment", "Foo \"Bar\" Baz"},
                new[]
                {
                    new[] {"-A", "INPUT"}, new[] {"-j", "ACCEPT"}, new[] {"-m", "comment"},
                    new[] {"--comment", "Foo \"Bar\" Baz"},
                }
            },
            {
                "-A INPUT -j ACCEPT -m comment --comment \"Foo \\\\\\\"Bar\\\" Baz\"",
                new[] {"-A", "INPUT", "-j", "ACCEPT", "-m", "comment", "--comment", "Foo \\\"Bar\" Baz"},
                new[]
                {
                    new[] {"-A", "INPUT"}, new[] {"-j", "ACCEPT"}, new[] {"-m", "comment"},
                    new[] {"--comment", "Foo \\\"Bar\" Baz"},
                }
            },
            {
                "\"\"",
                new[] {""},
                new[] {new[] {""}}
            },
            {
                "=",
                new[] {"="},
                new[] {new[] {"="}}
            },
            {
                "",
                new string[] { },
                new string[][] { }
            },
        };

        public static TheoryData<string, Type> InvalidDefinitionsData => new TheoryData<string, Type>
        {
            {"\"", typeof(UnclosedStringException)},
            {"-A INPUT -j ACCEPT -m comment --comment \"Foo", typeof(UnclosedStringException)},
            {"-A INPUT -j ACCEPT -m comment --comment \"Foo\\\"", typeof(UnclosedStringException)},
        };
    }
}
