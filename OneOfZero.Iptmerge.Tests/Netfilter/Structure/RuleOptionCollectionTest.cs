using System.Collections.Generic;
using OneOfZero.Iptmerge.Netfilter.Structure;
using Xunit;

//
// SPDX-License-Identifier: MIT
// Find the full license text at: https://gitlab.com/1of0/csharp/iptmerge/-/blob/master/LICENSE.md
//

namespace OneOfZero.Iptmerge.Tests.Netfilter.Structure
{
    using OptionsByNameTheoryData = TheoryData<RuleOptionCollection, string, IEnumerable<RuleOption>>;
    using OptionsByNamesTheoryData = TheoryData<RuleOptionCollection, IEnumerable<string>, IEnumerable<RuleOption>>;
    using WithoutOptionTheoryData = TheoryData<RuleOptionCollection, RuleOption, RuleOptionCollection>;

    public class RuleOptionCollectionTest
    {
        [Theory]
        [MemberData(nameof(OptionsByNameData))]
        public void TestGetOptionsByName(
            RuleOptionCollection collection,
            string optionName,
            IEnumerable<RuleOption> expectedOptions
        )
        {
            Assert.Equal(expectedOptions, collection.GetOptions(optionName));
        }

        [Theory]
        [MemberData(nameof(OptionsByNamesData))]
        public void TestGetOptionsByNames(
            RuleOptionCollection collection,
            IEnumerable<string> optionNames,
            IEnumerable<RuleOption> expectedOptions
        )
        {
            Assert.Equal(expectedOptions, collection.GetOptions(optionNames));
        }

        [Theory]
        [MemberData(nameof(WithoutOptionData))]
        public void TestWithoutOption(
            RuleOptionCollection collection,
            RuleOption optionToRemove,
            RuleOptionCollection expectedCollection
        )
        {
            Assert.Equal(expectedCollection, collection.WithoutOption(optionToRemove));
        }

        [Fact]
        public void TestHashCode()
        {
            var a = new RuleOptionCollection(new[] {new RuleOption("a"), new RuleOption("b"), new RuleOption("a")});
            var b = new RuleOptionCollection(new[] {new RuleOption("a"), new RuleOption("b"), new RuleOption("a")});
            var c = new RuleOptionCollection(new[] {new RuleOption("a"), new RuleOption("a")});
            var d = new RuleOptionCollection(new RuleOption[]{});
            var e = new RuleOptionCollection();

            Assert.Equal(a.GetHashCode(), b.GetHashCode());
            Assert.NotEqual(a.GetHashCode(), c.GetHashCode());
            Assert.Equal(d.GetHashCode(), e.GetHashCode());
        }

        public static OptionsByNameTheoryData OptionsByNameData = new OptionsByNameTheoryData
        {
            {
                new RuleOptionCollection(new[] {new RuleOption("a"), new RuleOption("b"), new RuleOption("a")}),
                "a",
                new[] {new RuleOption("a"), new RuleOption("a")}
            },
            {
                new RuleOptionCollection(new[] {new RuleOption("a"), new RuleOption("b"), new RuleOption("a")}),
                "b",
                new[] {new RuleOption("b")}
            },
            {
                new RuleOptionCollection(new[] {new RuleOption("a"), new RuleOption("b"), new RuleOption("a")}),
                "c",
                new RuleOption[] { }
            },
        };

        public static OptionsByNamesTheoryData OptionsByNamesData = new OptionsByNamesTheoryData
        {
            {
                new RuleOptionCollection(new[] {new RuleOption("a"), new RuleOption("b"), new RuleOption("a")}),
                new[] {"a", "b"},
                new[] {new RuleOption("a"), new RuleOption("b"), new RuleOption("a")}
            },
            {
                new RuleOptionCollection(new[] {new RuleOption("a"), new RuleOption("b"), new RuleOption("a")}),
                new[] {"a"},
                new[] {new RuleOption("a"), new RuleOption("a")}
            },
            {
                new RuleOptionCollection(new[] {new RuleOption("a"), new RuleOption("b"), new RuleOption("a")}),
                new[] {"b"},
                new[] {new RuleOption("b")}
            },
            {
                new RuleOptionCollection(new[] {new RuleOption("a"), new RuleOption("b"), new RuleOption("a")}),
                new[] {"b", "c"},
                new[] {new RuleOption("b")}
            },
            {
                new RuleOptionCollection(new[] {new RuleOption("a"), new RuleOption("b"), new RuleOption("a")}),
                new[] {"c"},
                new RuleOption[] { }
            },
            {
                new RuleOptionCollection(new[] {new RuleOption("a"), new RuleOption("b"), new RuleOption("a")}),
                new string[] { },
                new RuleOption[] { }
            },
        };

        public static WithoutOptionTheoryData WithoutOptionData = new WithoutOptionTheoryData
        {
            {
                new RuleOptionCollection(new[] {new RuleOption("a"), new RuleOption("b"), new RuleOption("a")}),
                new RuleOption("b"),
                new RuleOptionCollection(new[] {new RuleOption("a"), new RuleOption("a")})
            },
            {
                new RuleOptionCollection(new[] {new RuleOption("a"), new RuleOption("b"), new RuleOption("a")}),
                new RuleOption("a"),
                new RuleOptionCollection(new[] {new RuleOption("b")})
            },
            {
                new RuleOptionCollection(new[] {new RuleOption("a"), new RuleOption("b"), new RuleOption("a")}),
                new RuleOption("c"),
                new RuleOptionCollection(new[] {new RuleOption("a"), new RuleOption("b"), new RuleOption("a")})
            },
        };
    }
}
