using System.Linq;
using OneOfZero.Iptmerge.Netfilter.Structure;
using Xunit;

//
// SPDX-License-Identifier: MIT
// Find the full license text at: https://gitlab.com/1of0/csharp/iptmerge/-/blob/master/LICENSE.md
//

namespace OneOfZero.Iptmerge.Tests.Netfilter.Structure
{
    public class StateTest
    {
        [Fact]
        public void TestGetTables()
        {
            var tables = new[]
            {
                new Table("foo"),
                new Table("bar"),
            };

            var state = new State(tables.ToList());
            var secondState = new State(tables.ToList());

            Assert.Equal(tables, state.Tables);
            Assert.Equal("*foo\nCOMMIT\n*bar\nCOMMIT\n", state.ToString());
            Assert.Equal(state.GetHashCode(), secondState.GetHashCode());
        }
    }
}
