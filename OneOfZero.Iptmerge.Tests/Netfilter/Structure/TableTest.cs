using System.Collections.Generic;
using OneOfZero.Iptmerge.Netfilter.Structure;
using Xunit;

//
// SPDX-License-Identifier: MIT
// Find the full license text at: https://gitlab.com/1of0/csharp/iptmerge/-/blob/master/LICENSE.md
//

namespace OneOfZero.Iptmerge.Tests.Netfilter.Structure
{
    using GetterTheoryData = TheoryData<Table, string, IEnumerable<Chain>, IEnumerable<Rule>, string>;

    public class TableTest
    {
        [Theory]
        [MemberData(nameof(GetterData))]
        public void TestGetters(
            Table table,
            string expectedName,
            IEnumerable<Chain> expectedChains,
            IEnumerable<Rule> expectedRules,
            string expectedToString
        )
        {
            Assert.Equal(expectedName, table.Name);
            Assert.Equal(expectedChains, table.Chains);
            Assert.Equal(expectedRules, table.Rules);
            Assert.Equal(expectedToString, table.ToString());

            var newTable = new Table(expectedName, expectedChains, expectedRules);
            Assert.Equal(expectedName, newTable.Name);
            Assert.Equal(expectedChains, newTable.Chains);
            Assert.Equal(expectedRules, newTable.Rules);
            Assert.Equal(expectedToString, newTable.ToString());

            Assert.Equal(table.GetHashCode(), newTable.GetHashCode());
        }

        public static GetterTheoryData GetterData = new GetterTheoryData
        {
            {
                new Table(
                    "filter",
                    new[] {new Chain("INPUT", "ACCEPT", "[0:0]")},
                    new[] {new Rule("A", "INPUT")}
                ),
                "filter",
                new[] {new Chain("INPUT", "ACCEPT", "[0:0]")},
                new[] {new Rule("A", "INPUT")},
                "*filter\n:INPUT ACCEPT [0:0]\n-A 'INPUT'\nCOMMIT\n"
            },
            {
                new Table(
                    "filter",
                    new[] {new Chain("INPUT", "ACCEPT", "[0:0]")},
                    new Rule[] { }
                ),
                "filter",
                new[] {new Chain("INPUT", "ACCEPT", "[0:0]")},
                new Rule[] { },
                "*filter\n:INPUT ACCEPT [0:0]\nCOMMIT\n"
            },
            {
                new Table(
                    "filter",
                    new Chain[] { },
                    new[] {new Rule("A", "INPUT")}
                ),
                "filter",
                new Chain[] { },
                new[] {new Rule("A", "INPUT")},
                "*filter\n-A 'INPUT'\nCOMMIT\n"
            },
            {
                new Table(
                    "filter",
                    new Chain[] { },
                    new Rule[] { }
                ),
                "filter",
                new Chain[] { },
                new Rule[] { },
                "*filter\nCOMMIT\n"
            },
        };
    }
}
