using OneOfZero.Iptmerge.Netfilter.Structure;
using Xunit;

//
// SPDX-License-Identifier: MIT
// Find the full license text at: https://gitlab.com/1of0/csharp/iptmerge/-/blob/master/LICENSE.md
//

namespace OneOfZero.Iptmerge.Tests.Netfilter.Structure
{
    using GetterTheoryData = TheoryData<Chain, string, string, string>;
    using ComparisonTheoryData = TheoryData<Chain, Chain, bool>;

    public class ChainTest
    {
        [Theory]
        [MemberData(nameof(GetterData))]
        public void TestGetters(
            Chain chain,
            string expectedName,
            string expectedPolicy,
            string expectedCounters
        )
        {
            Assert.Equal(expectedName, chain.Name);
            Assert.Equal(expectedPolicy, chain.Policy);
            Assert.Equal(expectedCounters, chain.Counters);

            var newChain = new Chain(expectedName, expectedPolicy, expectedCounters);

            Assert.Equal(expectedName, newChain.Name);
            Assert.Equal(expectedPolicy, newChain.Policy);
            Assert.Equal(expectedCounters, newChain.Counters);

            Assert.Equal(chain.GetHashCode(), newChain.GetHashCode());
        }

        [Fact]
        public void TestWithChain()
        {
            var chain = new Chain("foo", "ACCEPT", "[0:0]");

            Assert.Equal("[1:1]", chain.WithCounters("[1:1]").Counters);
        }

        [Theory]
        [MemberData(nameof(ComparisonData))]
        public void TestComparison(Chain a, Chain b, bool expectedEqual)
        {
            Assert.Equal(expectedEqual, a.Equals(b));
            Assert.Equal(expectedEqual, b.Equals(a));
            Assert.Equal(expectedEqual, a == b);
            Assert.Equal(expectedEqual, b == a);
            Assert.Equal(!expectedEqual, a != b);
            Assert.Equal(!expectedEqual, b != a);
        }

        public static GetterTheoryData GetterData = new GetterTheoryData
        {
            {new Chain("INPUT", "ACCEPT", "[0:0]"), "INPUT", "ACCEPT", "[0:0]"},
            {new Chain("INPUT", "-", "[0:0]"), "INPUT", "-", "[0:0]"},
        };

        public static ComparisonTheoryData ComparisonData = new ComparisonTheoryData
        {
            {new Chain("INPUT", "ACCEPT", "[0:0]"), new Chain("INPUT", "ACCEPT", "[0:0]"), true},
            {new Chain("INPUT", "ACCEPT", "[0:0]"), new Chain("INPUT", "DROP", "[0:0]"), true},
            {new Chain("INPUT", "ACCEPT", "[0:0]"), new Chain("INPUT", "ACCEPT", "[123:456]"), true},
            {new Chain("INPUT", "ACCEPT", "[0:0]"), new Chain("OUTPUT", "ACCEPT", "[0:0]"), false},
            {new Chain("INPUT", "ACCEPT", "[0:0]"), new Chain("OUTPUT", "DROP", "[0:0]"), false},
            {new Chain("INPUT", "ACCEPT", "[0:0]"), new Chain("OUTPUT", "ACCEPT", "[123:456]"), false},
        };
    }
}
