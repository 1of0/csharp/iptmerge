using OneOfZero.Iptmerge.Netfilter.Structure;
using Xunit;

//
// SPDX-License-Identifier: MIT
// Find the full license text at: https://gitlab.com/1of0/csharp/iptmerge/-/blob/master/LICENSE.md
//

namespace OneOfZero.Iptmerge.Tests.Netfilter.Structure
{
    using ComparisonTheoryData = TheoryData<RuleOption, RuleOption, bool>;

    public class RuleOptionTest
    {
        [Theory]
        [MemberData(nameof(ComparisonData))]
        public void TestComparison(RuleOption a, RuleOption b, bool expectedEqual)
        {
            Assert.Equal(expectedEqual, a.Equals(b));
            Assert.Equal(expectedEqual, b.Equals(a));
            Assert.Equal(expectedEqual, a == b);
            Assert.Equal(expectedEqual, b == a);
            Assert.Equal(!expectedEqual, a != b);
            Assert.Equal(!expectedEqual, b != a);

            if (expectedEqual)
            {
                Assert.Equal(a.GetHashCode(), b.GetHashCode());
            }
            else
            {
                Assert.NotEqual(a.GetHashCode(), b.GetHashCode());
            }
        }

        public static ComparisonTheoryData ComparisonData = new ComparisonTheoryData
        {
            {
                new RuleOption("foo", new[] {"bar", "baz"}),
                new RuleOption("foo", new[] {"bar", "baz"}),
                true
            },
            {
                new RuleOption("foo"),
                new RuleOption("foo"),
                true
            },
            {
                new RuleOption("foo"),
                new RuleOption("bar"),
                false
            },
            {
                new RuleOption("foo", new string[] { }),
                new RuleOption("foo", new string[] { }),
                true
            },
            {
                new RuleOption("foo", new[] {"bar", "baz"}),
                new RuleOption("foo", new string[] { }),
                false
            },
            {
                new RuleOption("foo", new[] {"bar", "baz"}),
                new RuleOption("bar", new[] {"bar", "baz"}),
                false
            },
            {
                new RuleOption("foo", new[] {"bar", "baz"}),
                new RuleOption("foo", new[] {"bar", "baz"}, true),
                false
            },
            {
                new RuleOption("foo", new[] {"bar", "baz"}),
                new RuleOption("foo", new[] {"bar"}),
                false
            },
        };
    }
}
