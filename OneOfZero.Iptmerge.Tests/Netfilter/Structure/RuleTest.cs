using System.Collections.Generic;
using OneOfZero.Iptmerge.Netfilter.Structure;
using Xunit;

//
// SPDX-License-Identifier: MIT
// Find the full license text at: https://gitlab.com/1of0/csharp/iptmerge/-/blob/master/LICENSE.md
//

namespace OneOfZero.Iptmerge.Tests.Netfilter.Structure
{
    using GetterTheoryData = TheoryData<Rule, string, string, IEnumerable<RuleOption>, string>;
    using ReplacedOptionTheoryData = TheoryData<Rule, string, RuleOption, string>;

    public class RuleTest
    {
        [Theory]
        [MemberData(nameof(GetterData))]
        public void TestGetters(
            Rule rule,
            string expectedAction,
            string expectedChain,
            IEnumerable<RuleOption> expectedOptions,
            string expectedString
        )
        {
            Assert.Equal(expectedAction, rule.Action);
            Assert.Equal(expectedChain, rule.Chain);
            Assert.Equal(expectedOptions, rule.GetOptions());
            Assert.Equal(expectedString, rule.ToString());
        }

        [Theory]
        [MemberData(nameof(ComparisonData))]
        public void TestComparison(Rule a, Rule b, bool expectedEqual)
        {
            Assert.Equal(expectedEqual, a.Equals(b));
            Assert.Equal(expectedEqual, b.Equals(a));
            Assert.Equal(expectedEqual, a == b);
            Assert.Equal(expectedEqual, b == a);
            Assert.Equal(!expectedEqual, a != b);
            Assert.Equal(!expectedEqual, b != a);

            if (expectedEqual)
            {
                Assert.Equal(a.GetHashCode(), b.GetHashCode());
            }
            else
            {
                Assert.NotEqual(a.GetHashCode(), b.GetHashCode());
            }
        }

        [Theory]
        [MemberData(nameof(ReplacedOptionData))]
        public void TestWithReplacedOption(Rule rule, string optionName, RuleOption replacement, string expectedString)
        {
            Assert.Equal(expectedString, rule.WithReplacedOption(optionName, replacement).ToString());
        }

        [Theory]
        [MemberData(nameof(ActionData))]
        public void TestWithAction(Rule rule, string actionReplacement, Rule expectedRule)
        {
            Assert.Equal(expectedRule, rule.WithAction(actionReplacement));
        }

        public static GetterTheoryData GetterData = new GetterTheoryData
        {
            {
                new Rule("A", "INPUT"),
                "A",
                "INPUT",
                new RuleOption[] { },
                "-A 'INPUT'"
            },
            {
                new Rule("A", "INPUT", new[] {new RuleOption("s", "127.0.0.1")}),
                "A",
                "INPUT",
                new[] {new RuleOption("s", "127.0.0.1")},
                "-A 'INPUT' -s '127.0.0.1/32'"
            },
        };

        public static TheoryData<Rule, Rule, bool> ComparisonData = new TheoryData<Rule, Rule, bool>
        {
            {
                new Rule("A", "INPUT", new[] {new RuleOption("s", "127.0.0.1")}),
                new Rule("A", "INPUT", new[] {new RuleOption("s", "127.0.0.1")}),
                true
            },
            {
                new Rule("A", "INPUT", new[] {new RuleOption("s", "127.1")}),
                new Rule("A", "INPUT", new[] {new RuleOption("s", "127.0.0.1")}),
                true
            },
            {
                new Rule("A", "INPUT", new[] {new RuleOption("s", "127.0.0.1/32")}),
                new Rule("A", "INPUT", new[] {new RuleOption("s", "127.0.0.1")}),
                true
            },
            {
                new Rule("A", "INPUT", new[] {new RuleOption("s", "127.0.0.1")}),
                new Rule("I", "INPUT", new[] {new RuleOption("s", "127.0.0.1")}),
                true
            },
            {
                new Rule("A", "INPUT", new[] {new RuleOption("s", "127.0.0.1")}),
                new Rule("A", "OUTPUT", new[] {new RuleOption("s", "127.0.0.1")}),
                false
            },
            {
                new Rule("A", "INPUT", new[] {new RuleOption("s", "127.0.0.1")}),
                new Rule("A", "INPUT", new[] {new RuleOption("d", "127.0.0.1")}),
                false
            },
            {
                new Rule("A", "INPUT", new[] {new RuleOption("s", "127.0.0.1")}),
                new Rule("A", "INPUT", new[] {new RuleOption("s", "127.0.0.1"), new RuleOption("d", "127.0.0.1")}),
                false
            },
            {
                new Rule("A", "INPUT", new[] {new RuleOption("d", "127.0.0.1"), new RuleOption("s", "127.0.0.1")}),
                new Rule("A", "INPUT", new[] {new RuleOption("s", "127.0.0.1"), new RuleOption("d", "127.0.0.1")}),
                true
            },
        };

        public static ReplacedOptionTheoryData ReplacedOptionData = new ReplacedOptionTheoryData
        {
            {
                new Rule("A", "INPUT", new[] {new RuleOption("s", "127.0.0.1")}),
                "s",
                new RuleOption("d", "127.0.0.1"),
                "-A 'INPUT' -d '127.0.0.1/32'"
            },
            {
                new Rule("A", "INPUT", new[] {new RuleOption("s", "127.0.0.1"), new RuleOption("s", "127.0.0.1")}),
                "s",
                new RuleOption("d", "127.0.0.1"),
                "-A 'INPUT' -d '127.0.0.1/32' -d '127.0.0.1/32'"
            },
            {
                new Rule("A", "INPUT", new[] {new RuleOption("s", "127.0.0.1")}),
                "d",
                new RuleOption("d", "127.0.0.1"),
                "-A 'INPUT' -s '127.0.0.1/32'"
            },
        };

        public static TheoryData<Rule, string, Rule> ActionData = new TheoryData<Rule, string, Rule>
        {
            {
                new Rule("A", "INPUT"),
                "delete",
                new Rule("delete", "INPUT")
            },
        };
    }
}
