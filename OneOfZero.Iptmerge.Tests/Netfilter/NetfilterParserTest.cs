using System.IO;
using OneOfZero.Iptmerge.Exceptions;
using OneOfZero.Iptmerge.Netfilter;
using Xunit;

//
// SPDX-License-Identifier: MIT
// Find the full license text at: https://gitlab.com/1of0/csharp/iptmerge/-/blob/master/LICENSE.md
//

namespace OneOfZero.Iptmerge.Tests.Netfilter
{
    public class NetfilterParserTest
    {
        [Fact]
        public void TestAll()
        {
            var parser = new NetfilterParser();
            Assert.NotNull(parser.ParseFile("Fixture/generated.state"));
            Assert.NotNull(parser.ParseString(File.ReadAllText("Fixture/generated.state")));
            Assert.Throws<ParseException>(() => parser.ParseFile("Fixture/non-existent.state"));
        }
    }
}
