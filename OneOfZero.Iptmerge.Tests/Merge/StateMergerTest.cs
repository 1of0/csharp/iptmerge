using System.Collections.Immutable;
using OneOfZero.Iptmerge.Merge;
using OneOfZero.Iptmerge.Merge.Configuration;
using OneOfZero.Iptmerge.Netfilter;
using OneOfZero.Iptmerge.Netfilter.Structure;
using Xunit;

//
// SPDX-License-Identifier: MIT
// Find the full license text at: https://gitlab.com/1of0/csharp/iptmerge/-/blob/master/LICENSE.md
//

namespace OneOfZero.Iptmerge.Tests.Merge
{
    using BasicMergeTheoryData = TheoryData<MergeBehaviour, string, string?>;

    public class StateMergerTest
    {
        [Theory]
        [MemberData(nameof(BasicMergeData))]
        public void TestMergeBasic(
            MergeBehaviour mergeBehaviour,
            string expectedStateName,
            string? generatedStateName = null
        )
        {
            var baseState = LoadFixture("running");
            var supplicantState = LoadFixture(generatedStateName ?? "generated");

            var merger = new StateMerger();

            var mergedState = merger.Merge(baseState, supplicantState, mergeBehaviour);

            Assert.Equal(LoadFixture(expectedStateName), mergedState);
        }

        private State LoadFixture(string name)
        {
            return new NetfilterParser().ParseFile($"Fixture/{name}.state");
        }

        public static BasicMergeTheoryData BasicMergeData = new BasicMergeTheoryData
        {
            {
                new MergeBehaviour
                {
                    Managed = new Managed
                    {
                        ChainPattern = "^a-managed",
                        RulePattern = "^b-managed",
                    },
                },
                "merged.keep.keep",
                null
            },
            {
                new MergeBehaviour
                {
                    Managed = new Managed
                    {
                        ChainPattern = "^a-managed",
                        RulePattern = "^b-managed",
                    },
                },
                "merged.only-filter",
                "generated.only-filter"
            },
            {
                new MergeBehaviour
                {
                    Managed = new Managed
                    {
                        ChainPattern = "^a-managed",
                        RulePattern = "^b-managed",
                    },
                    Unmanaged = new Unmanaged
                    {
                        RuleStrategy = StrategyKind.Purge,
                        ChainStrategy = StrategyKind.Purge,
                    },
                },
                "merged.purge.purge",
                null
            },
            {
                new MergeBehaviour
                {
                    Managed = new Managed
                    {
                        ChainPattern = "^a-managed",
                        RulePattern = "^b-managed",
                    },
                    Unmanaged = new Unmanaged
                    {
                        ChainStrategy = StrategyKind.Whitelist,
                        ChainWhitelists = new[] {"^foo"}.ToImmutableArray(),
                    },
                },
                "merged.whitelist.keep",
                null
            },
            {
                new MergeBehaviour
                {
                    Managed = new Managed
                    {
                        ChainPattern = "^a-managed",
                        RulePattern = "^b-managed",
                    },
                    Unmanaged = new Unmanaged
                    {
                        RuleStrategy = StrategyKind.Whitelist,
                        RuleWhitelists = new[] {"^foo"}.ToImmutableArray(),
                    },
                },
                "merged.keep.whitelist",
                null
            },
            {
                new MergeBehaviour
                {
                    Managed = new Managed
                    {
                        ChainPattern = "^a-managed",
                        RulePattern = "^b-managed",
                    },
                    Unmanaged = new Unmanaged
                    {
                        ChainStrategy = StrategyKind.Whitelist,
                        ChainWhitelists = new[] {"^foo"}.ToImmutableArray(),
                        RuleStrategy = StrategyKind.Whitelist,
                        RuleWhitelists = new[] {"^foo"}.ToImmutableArray(),
                    },
                },
                "merged.whitelist.whitelist",
                null
            },
            {
                new MergeBehaviour
                {
                    Managed = new Managed
                    {
                        ChainPattern = "^a-managed",
                        RulePattern = "^b-managed",
                    },
                    Unmanaged = new Unmanaged
                    {
                        RuleMatchAgainst = MatchKind.Full,
                        RuleStrategy = StrategyKind.Whitelist,
                        RuleWhitelists = new[] {@"127\.1\."}.ToImmutableArray(),
                    },
                },
                "merged.full-rule-match",
                null
            },
        };
    }
}
