using System;
using System.Diagnostics.CodeAnalysis;

//
// SPDX-License-Identifier: MIT
// Find the full license text at: https://gitlab.com/1of0/csharp/iptmerge/-/blob/master/LICENSE.md
//

namespace OneOfZero.Iptmerge.Exceptions
{
    [ExcludeFromCodeCoverage]
    public class UnclosedStringException : ParseException
    {
        public UnclosedStringException()
        {
        }

        public UnclosedStringException(string? message) : base(message)
        {
        }

        public UnclosedStringException(string? message, Exception? innerException) : base(message, innerException)
        {
        }
    }
}
