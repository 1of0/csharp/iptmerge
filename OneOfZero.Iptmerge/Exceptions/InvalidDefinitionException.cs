using System;
using System.Diagnostics.CodeAnalysis;

//
// SPDX-License-Identifier: MIT
// Find the full license text at: https://gitlab.com/1of0/csharp/iptmerge/-/blob/master/LICENSE.md
//

namespace OneOfZero.Iptmerge.Exceptions
{
    [ExcludeFromCodeCoverage]
    public class InvalidDefinitionException : ParseException
    {
        public InvalidDefinitionException()
        {
        }

        public InvalidDefinitionException(string? message) : base(message)
        {
        }

        public InvalidDefinitionException(string? message, Exception? innerException) : base(message, innerException)
        {
        }
    }
}
