using System;
using System.Diagnostics.CodeAnalysis;

//
// SPDX-License-Identifier: MIT
// Find the full license text at: https://gitlab.com/1of0/csharp/iptmerge/-/blob/master/LICENSE.md
//

namespace OneOfZero.Iptmerge.Exceptions
{
    [ExcludeFromCodeCoverage]
    public class IptmergeException : Exception
    {
        public IptmergeException()
        {
        }

        public IptmergeException(string? message) : base(message)
        {
        }

        public IptmergeException(string? message, Exception? innerException) : base(message, innerException)
        {
        }
    }
}
