using System.Collections.Generic;
using System.Linq;
using OneOfZero.Iptmerge.Merge.Configuration;
using OneOfZero.Iptmerge.Merge.Strategy;
using OneOfZero.Iptmerge.Netfilter.Structure;

//
// SPDX-License-Identifier: MIT
// Find the full license text at: https://gitlab.com/1of0/csharp/iptmerge/-/blob/master/LICENSE.md
//

namespace OneOfZero.Iptmerge.Merge
{
    using StrategyMap = Dictionary<StrategyKind, IStrategy>;

    public class StateMerger
    {
        private static readonly StrategyMap ChainStrategyMapping = new StrategyMap
        {
            {StrategyKind.Keep, new ChainKeep()},
            {StrategyKind.Purge, new ChainPurge()},
            {StrategyKind.Whitelist, new ChainWhitelist()},
        };

        private static readonly StrategyMap RuleStrategyMapping = new StrategyMap
        {
            {StrategyKind.Keep, new RuleKeep()},
            {StrategyKind.Purge, new RulePurge()},
            {StrategyKind.Whitelist, new RuleWhitelist()},
        };

        public State Merge(State baseState, State supplicantState, MergeBehaviour mergeBehaviour)
        {
            var baseTables = baseState.Tables.ToDictionary(t => t.Name);
            var supplicantTables = supplicantState.Tables.ToDictionary(t => t.Name);

            var mergedTables = new List<Table>();

            foreach (var (tableName, baseTable) in baseTables)
            {
                if (!supplicantTables.TryGetValue(tableName, out var supplicantTable))
                {
                    supplicantTable = new Table(tableName);
                }

                supplicantTable = TransplantCounters(baseTable, supplicantTable);
                supplicantTable = MergeChains(baseTable, supplicantTable, mergeBehaviour);
                supplicantTable = MergeRules(baseTable, supplicantTable, mergeBehaviour);

                mergedTables.Add(supplicantTable);
            }

            return new State(mergedTables);
        }

        private Table MergeChains(Table baseTable, Table supplicantTable, MergeBehaviour mergeBehaviour)
        {
            var strategy = ChainStrategyMapping[mergeBehaviour.Unmanaged.ChainStrategy];
            return strategy.Merge(baseTable, supplicantTable, mergeBehaviour);
        }

        private Table MergeRules(Table baseTable, Table supplicantTable, MergeBehaviour mergeBehaviour)
        {
            var strategy = RuleStrategyMapping[mergeBehaviour.Unmanaged.RuleStrategy];
            return strategy.Merge(baseTable, supplicantTable, mergeBehaviour);
        }

        private Table TransplantCounters(Table baseTable, Table supplicantTable)
        {
            var baseChains = baseTable.Chains.ToDictionary(c => c.Name);

            var transplantedChains = supplicantTable.Chains.Select(supplicantChain =>
            {
                if (baseChains.TryGetValue(supplicantChain.Name, out var baseChain))
                {
                    return supplicantChain.WithCounters(baseChain.Counters);
                }

                return supplicantChain;
            });

            return new Table(
                supplicantTable.Name,
                transplantedChains,
                supplicantTable.Rules
            );
        }
    }
}
