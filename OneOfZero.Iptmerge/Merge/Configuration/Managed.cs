//
// SPDX-License-Identifier: MIT
// Find the full license text at: https://gitlab.com/1of0/csharp/iptmerge/-/blob/master/LICENSE.md
//

namespace OneOfZero.Iptmerge.Merge.Configuration
{
    public class Managed
    {
        public string ChainPattern { get; set; } = "";

        public string RulePattern { get; set; } = "";
    }
}
