//
// SPDX-License-Identifier: MIT
// Find the full license text at: https://gitlab.com/1of0/csharp/iptmerge/-/blob/master/LICENSE.md
//

namespace OneOfZero.Iptmerge.Merge.Configuration
{
    public class MergeBehaviour
    {
        public Managed Managed { get; set; } = new Managed();

        public Unmanaged Unmanaged { get; set; } = new Unmanaged();
    }
}
