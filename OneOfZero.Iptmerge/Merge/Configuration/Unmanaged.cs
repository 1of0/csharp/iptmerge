using System.Collections.Immutable;

//
// SPDX-License-Identifier: MIT
// Find the full license text at: https://gitlab.com/1of0/csharp/iptmerge/-/blob/master/LICENSE.md
//

namespace OneOfZero.Iptmerge.Merge.Configuration
{
    public class Unmanaged
    {
        public StrategyKind ChainStrategy { get; set; } = StrategyKind.Keep;

        public StrategyKind RuleStrategy { get; set; } = StrategyKind.Keep;

        public MatchKind RuleMatchAgainst { get; set; } = MatchKind.Comment;

        public ImmutableArray<string> ChainWhitelists { get; set; } = ImmutableArray<string>.Empty;

        public ImmutableArray<string> RuleWhitelists { get; set; } = ImmutableArray<string>.Empty;
    }
}
