using System.Linq;
using OneOfZero.Iptmerge.Merge.Configuration;
using OneOfZero.Iptmerge.Netfilter.Structure;

//
// SPDX-License-Identifier: MIT
// Find the full license text at: https://gitlab.com/1of0/csharp/iptmerge/-/blob/master/LICENSE.md
//

namespace OneOfZero.Iptmerge.Merge.Strategy
{
    public class ChainKeep : IStrategy
    {
        public Table Merge(Table baseTable, Table supplicantTable, MergeBehaviour mergeBehaviour)
        {
            var managedChains = baseTable.Chains.GetManaged(mergeBehaviour);

            var extraneousManagedChains = managedChains.Except(supplicantTable.Chains).ToList();

            var baseChainsWithoutExtraneousChains = baseTable.Chains.Except(extraneousManagedChains);

            var chainDeletions = extraneousManagedChains.GetDeletionRules();

            return new Table(
                supplicantTable.Name,
                baseChainsWithoutExtraneousChains.Union(supplicantTable.Chains),
                chainDeletions.Union(supplicantTable.Rules)
            );
        }
    }
}
