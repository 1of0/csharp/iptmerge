using System.Linq;
using OneOfZero.Iptmerge.Merge.Configuration;
using OneOfZero.Iptmerge.Netfilter.Structure;

//
// SPDX-License-Identifier: MIT
// Find the full license text at: https://gitlab.com/1of0/csharp/iptmerge/-/blob/master/LICENSE.md
//

namespace OneOfZero.Iptmerge.Merge.Strategy
{
    public class RulePurge : IStrategy
    {
        public Table Merge(Table baseTable, Table supplicantTable, MergeBehaviour mergeBehaviour)
        {
            var extraneousRules = baseTable.Rules.Except(supplicantTable.Rules);

            var ruleDeletions = extraneousRules.GetDeletionRules();

            var supplicantAdditions = supplicantTable.Rules.Except(baseTable.Rules);

            return new Table(
                supplicantTable.Name,
                supplicantTable.Chains,
                ruleDeletions.Union(supplicantAdditions)
            );
        }
    }
}
