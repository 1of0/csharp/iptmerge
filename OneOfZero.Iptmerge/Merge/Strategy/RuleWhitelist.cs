using System.Linq;
using OneOfZero.Iptmerge.Merge.Configuration;
using OneOfZero.Iptmerge.Netfilter.Structure;

//
// SPDX-License-Identifier: MIT
// Find the full license text at: https://gitlab.com/1of0/csharp/iptmerge/-/blob/master/LICENSE.md
//

namespace OneOfZero.Iptmerge.Merge.Strategy
{
    public class RuleWhitelist : IStrategy
    {
        public Table Merge(Table baseTable, Table supplicantTable, MergeBehaviour mergeBehaviour)
        {
            var extraneousRules = baseTable.Rules.Except(supplicantTable.Rules).ToList();

            var whitelisted = extraneousRules.GetWhitelisted(mergeBehaviour);

            var baseRulesToRemove = extraneousRules.Except(whitelisted);

            var ruleDeletions = baseRulesToRemove.GetDeletionRules();

            var supplicantAdditions = supplicantTable.Rules.Except(baseTable.Rules);

            return new Table(
                supplicantTable.Name,
                supplicantTable.Chains,
                ruleDeletions.Union(supplicantAdditions)
            );
        }
    }
}
