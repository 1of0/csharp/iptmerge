using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using OneOfZero.Iptmerge.Merge.Configuration;
using OneOfZero.Iptmerge.Netfilter.Structure;

//
// SPDX-License-Identifier: MIT
// Find the full license text at: https://gitlab.com/1of0/csharp/iptmerge/-/blob/master/LICENSE.md
//

namespace OneOfZero.Iptmerge.Merge.Strategy
{
    public static class StrategyHelpers
    {
        public static IEnumerable<Chain> GetManaged(this IEnumerable<Chain> chains, MergeBehaviour mergeBehaviour)
        {
            return chains.Where(chain => Regex.IsMatch(chain.Name, mergeBehaviour.Managed.ChainPattern));
        }

        public static IEnumerable<Rule> GetManaged(this IEnumerable<Rule> rules, MergeBehaviour mergeBehaviour)
        {
            return rules.Where(rule =>
            {
                var commentOption = rule.GetOptions("comment").FirstOrDefault();
                var commentValue = commentOption?.Values.FirstOrDefault() ?? "";
                return Regex.IsMatch(commentValue, mergeBehaviour.Managed.RulePattern);
            });
        }

        public static IEnumerable<Chain> GetWhitelisted(this IEnumerable<Chain> chains, MergeBehaviour mergeBehaviour)
        {
            return chains.Where(chain =>
            {
                return mergeBehaviour.Unmanaged.ChainWhitelists
                    .Any(whitelistPattern => Regex.IsMatch(chain.Name, whitelistPattern));
            });
        }

        public static IEnumerable<Rule> GetWhitelisted(this IEnumerable<Rule> rules, MergeBehaviour mergeBehaviour)
        {
            return rules.Where(rule =>
            {
                string matchAgainst = "";

                switch (mergeBehaviour.Unmanaged.RuleMatchAgainst)
                {
                    case MatchKind.Comment:
                        var commentOption = rule.GetOptions("comment").FirstOrDefault();
                        matchAgainst = commentOption?.Values.FirstOrDefault() ?? "";
                        break;
                    case MatchKind.Full:
                        matchAgainst = rule.ToString();
                        break;
                }

                return mergeBehaviour.Unmanaged.RuleWhitelists
                    .Any(whitelistPattern => Regex.IsMatch(matchAgainst, whitelistPattern));
            });
        }

        public static IEnumerable<Rule> GetDeletionRules(this IEnumerable<Chain> chains)
        {
            return chains.Select(chain => new Rule("delete-chain", chain.Name));
        }

        public static IEnumerable<Rule> GetDeletionRules(this IEnumerable<Rule> rules)
        {
            return rules.Select(rule => rule.WithAction("delete"));
        }
    }
}
