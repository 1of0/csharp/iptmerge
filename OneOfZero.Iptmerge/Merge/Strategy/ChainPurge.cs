using System.Linq;
using OneOfZero.Iptmerge.Merge.Configuration;
using OneOfZero.Iptmerge.Netfilter.Structure;

//
// SPDX-License-Identifier: MIT
// Find the full license text at: https://gitlab.com/1of0/csharp/iptmerge/-/blob/master/LICENSE.md
//

namespace OneOfZero.Iptmerge.Merge.Strategy
{
    public class ChainPurge : IStrategy
    {
        public Table Merge(Table baseTable, Table supplicantTable, MergeBehaviour mergeBehaviour)
        {
            var extraneousChains = baseTable.Chains.Except(supplicantTable.Chains);

            var chainDeletions = extraneousChains.GetDeletionRules();

            return new Table(
                supplicantTable.Name,
                supplicantTable.Chains,
                chainDeletions.Union(supplicantTable.Rules)
            );
        }
    }
}
