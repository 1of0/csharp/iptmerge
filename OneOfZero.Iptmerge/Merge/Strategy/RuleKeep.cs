using System.Linq;
using OneOfZero.Iptmerge.Merge.Configuration;
using OneOfZero.Iptmerge.Netfilter.Structure;

//
// SPDX-License-Identifier: MIT
// Find the full license text at: https://gitlab.com/1of0/csharp/iptmerge/-/blob/master/LICENSE.md
//

namespace OneOfZero.Iptmerge.Merge.Strategy
{
    public class RuleKeep : IStrategy
    {
        public Table Merge(Table baseTable, Table supplicantTable, MergeBehaviour mergeBehaviour)
        {
            var managedRules = baseTable.Rules.GetManaged(mergeBehaviour);

            var extraneousManagedRules = managedRules.Except(supplicantTable.Rules).ToList();

            var baseRulesWithoutExtraneousRules = baseTable.Rules.Except(extraneousManagedRules);

            var ruleDeletions = extraneousManagedRules.GetDeletionRules();

            var supplicantAdditions = supplicantTable.Rules.Except(baseRulesWithoutExtraneousRules);

            return new Table(
                supplicantTable.Name,
                supplicantTable.Chains,
                ruleDeletions.Union(supplicantAdditions)
            );
        }
    }
}
