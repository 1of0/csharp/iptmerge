using System.Linq;
using OneOfZero.Iptmerge.Merge.Configuration;
using OneOfZero.Iptmerge.Netfilter.Structure;

//
// SPDX-License-Identifier: MIT
// Find the full license text at: https://gitlab.com/1of0/csharp/iptmerge/-/blob/master/LICENSE.md
//

namespace OneOfZero.Iptmerge.Merge.Strategy
{
    public class ChainWhitelist : IStrategy
    {
        public Table Merge(Table baseTable, Table supplicantTable, MergeBehaviour mergeBehaviour)
        {
            var extraneousChains = baseTable.Chains.Except(supplicantTable.Chains).ToList();

            var whitelisted = extraneousChains.GetWhitelisted(mergeBehaviour).ToList();

            var baseChainsToRemove = extraneousChains.Except(whitelisted);

            var chainDeletions = baseChainsToRemove.GetDeletionRules();

            return new Table(
                supplicantTable.Name,
                whitelisted.Union(supplicantTable.Chains),
                chainDeletions.Union(supplicantTable.Rules)
            );
        }
    }
}
