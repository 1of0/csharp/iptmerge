using OneOfZero.Iptmerge.Merge.Configuration;
using OneOfZero.Iptmerge.Netfilter.Structure;

//
// SPDX-License-Identifier: MIT
// Find the full license text at: https://gitlab.com/1of0/csharp/iptmerge/-/blob/master/LICENSE.md
//

namespace OneOfZero.Iptmerge.Merge.Strategy
{
    public interface IStrategy
    {
        public Table Merge(Table baseTable, Table supplicantTable, MergeBehaviour mergeBehaviour);
    }
}
