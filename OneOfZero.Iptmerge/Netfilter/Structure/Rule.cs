using System.Collections.Generic;
using System.Linq;

//
// SPDX-License-Identifier: MIT
// Find the full license text at: https://gitlab.com/1of0/csharp/iptmerge/-/blob/master/LICENSE.md
//

namespace OneOfZero.Iptmerge.Netfilter.Structure
{
    public class Rule : RuleOptionCollection
    {
        public string Action { get; }

        public string Chain { get; }

        public Rule(string action, string chain)
            : this(action, chain, new RuleOption[] { })
        {
        }

        public Rule(string action, string chain, IEnumerable<RuleOption> items)
            : base(items)
        {
            Action = action;
            Chain = chain;
        }

        public Rule WithAction(string action)
        {
            return new Rule(action, Chain, Items);
        }

        public Rule WithReplacedOption(string optionName, RuleOption replacement)
        {
            return new Rule(Action, Chain, Items.Select(o => o.Name == optionName ? replacement : o));
        }

        private RuleOption GetActionOption()
        {
            return new RuleOption(Action, Chain);
        }

        public override string ToString()
        {
            return Items.Count > 0
                ? $"{GetActionOption()} {base.ToString()}"
                : GetActionOption().ToString();
        }

        public override bool Equals(RuleOptionCollection? other)
        {
            return base.Equals(other)
                && other is Rule otherRule
                && otherRule.Chain == Chain;
        }

        public override int GetHashCode()
        {
            return $"{Chain}::{string.Join(' ', Items.OrderBy(o => o.ToString()))}".GetHashCode();
        }
    }
}
