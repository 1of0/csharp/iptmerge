using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using OneOfZero.Iptmerge.Util;

//
// SPDX-License-Identifier: MIT
// Find the full license text at: https://gitlab.com/1of0/csharp/iptmerge/-/blob/master/LICENSE.md
//

namespace OneOfZero.Iptmerge.Netfilter.Structure
{
    public class Table : AutoEqualityOperators<Table>
    {
        public string Name { get; }

        public ImmutableList<Chain> Chains { get; }

        public ImmutableList<Rule> Rules { get; }

        public Table(string name)
            : this(name, new Chain[] { }, new Rule[] { })
        {
        }

        public Table(string name, IEnumerable<Chain> chains, IEnumerable<Rule> rules)
        {
            Name = name;
            Chains = chains.ToImmutableList();
            Rules = rules.ToImmutableList();
        }

        public override string ToString()
        {
            var chains = string.Concat(Chains.Select(c => $"{c}\n"));
            var rules = string.Concat(Rules.Select(r => $"{r}\n"));

            return $"*{Name}\n{chains}{rules}COMMIT\n";
        }

        public override bool Equals(Table? other)
        {
            return other != null
               && other.Name == Name
               && other.Chains.SequenceEqual(Chains)
               && other.Rules.SequenceEqual(Rules);
        }

        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }
    }
}
