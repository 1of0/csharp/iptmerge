using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using OneOfZero.Iptmerge.Netfilter.Parse;
using OneOfZero.Iptmerge.Util;

//
// SPDX-License-Identifier: MIT
// Find the full license text at: https://gitlab.com/1of0/csharp/iptmerge/-/blob/master/LICENSE.md
//

namespace OneOfZero.Iptmerge.Netfilter.Structure
{
    public class RuleOption : AutoEqualityOperators<RuleOption>
    {
        public RuleOption(string name)
            : this(name, new string[] { })
        {
        }

        public RuleOption(string name, string value, bool negated = false)
            : this(name, new[] {value}, negated)
        {
        }

        public RuleOption(string name, IEnumerable<string> values, bool negated = false)
        {
            Name = name;
            Values = values.Select(v => v.Canonicalize()).ToImmutableList();
            Negated = negated;
        }

        public string Name { get; }

        public ImmutableList<string> Values { get; }

        public bool Negated { get; }

        private string GetOptionArgument()
        {
            return Name.Length > 1
                ? $"--{Name}"
                : $"-{Name}";
        }

        public override string ToString()
        {
            return string.Format(
                "{0}{1} {2}",
                Negated ? "! " : "",
                GetOptionArgument(),
                string.Join(' ', Values.Select(s => s.ShellEscape()))
            );
        }

        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }

        public override bool Equals(RuleOption? other)
        {
            return other != null
               && other.Name == Name
               && other.Negated == Negated
               && other.Values.SequenceEqual(Values);
        }
    }
}
