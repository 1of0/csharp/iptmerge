using System.Collections.Generic;
using System.Linq;
using OneOfZero.Iptmerge.Util;

//
// SPDX-License-Identifier: MIT
// Find the full license text at: https://gitlab.com/1of0/csharp/iptmerge/-/blob/master/LICENSE.md
//

namespace OneOfZero.Iptmerge.Netfilter.Structure
{
    public class RuleOptionCollection : AutoEqualityOperators<RuleOptionCollection>
    {
        protected readonly List<RuleOption> Items;

        public RuleOptionCollection()
            : this(new RuleOption[] { })
        {
        }

        public RuleOptionCollection(IEnumerable<RuleOption> items)
        {
            Items = items.ToList();
        }

        public IEnumerable<RuleOption> GetOptions()
        {
            return Items.ToList();
        }

        public IEnumerable<RuleOption> GetOptions(string optionName)
        {
            return Items.Where(o => o.Name == optionName);
        }

        public IEnumerable<RuleOption> GetOptions(IEnumerable<string> optionNames)
        {
            return Items.Where(o => optionNames.Contains(o.Name));
        }

        public RuleOptionCollection WithoutOption(RuleOption option)
        {
            return new RuleOptionCollection(Items.Where(o => o != option));
        }

        public override string ToString()
        {
            return string.Join(' ', Items);
        }

        public override bool Equals(RuleOptionCollection? other)
        {
            return other != null
                && other.Items.OrderBy(o => o.ToString()).SequenceEqual(Items.OrderBy(o => o.ToString()));
        }

        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }
    }
}
