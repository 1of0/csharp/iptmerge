using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using OneOfZero.Iptmerge.Util;

//
// SPDX-License-Identifier: MIT
// Find the full license text at: https://gitlab.com/1of0/csharp/iptmerge/-/blob/master/LICENSE.md
//

namespace OneOfZero.Iptmerge.Netfilter.Structure
{
    public class State : AutoEqualityOperators<State>
    {
        public ImmutableList<Table> Tables { get; }

        public State(IEnumerable<Table> tables)
        {
            Tables = tables.ToImmutableList();
        }

        public override string ToString()
        {
            return string.Concat(Tables);
        }

        public override bool Equals(State? other)
        {
            return other != null
                && other.Tables.SequenceEqual(Tables);
        }

        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }
    }
}
