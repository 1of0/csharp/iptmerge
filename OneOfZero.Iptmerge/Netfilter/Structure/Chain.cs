using OneOfZero.Iptmerge.Util;

//
// SPDX-License-Identifier: MIT
// Find the full license text at: https://gitlab.com/1of0/csharp/iptmerge/-/blob/master/LICENSE.md
//

namespace OneOfZero.Iptmerge.Netfilter.Structure
{
    public class Chain : AutoEqualityOperators<Chain>
    {
        public Chain(string name, string policy, string counters)
        {
            Name = name;
            Policy = policy;
            Counters = counters;
        }

        public string Name { get; }

        public string Policy { get; }

        public string Counters { get; }

        public Chain WithCounters(string counters)
        {
            return new Chain(Name, Policy, counters);
        }

        public override string ToString()
        {
            return $":{Name} {Policy} {Counters}";
        }

        public override bool Equals(Chain? other)
        {
            return other != null
                   && other.Name == Name;
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }
    }
}
