using System.IO;
using OneOfZero.Iptmerge.Exceptions;
using OneOfZero.Iptmerge.Netfilter.Parse;
using OneOfZero.Iptmerge.Netfilter.Structure;

//
// SPDX-License-Identifier: MIT
// Find the full license text at: https://gitlab.com/1of0/csharp/iptmerge/-/blob/master/LICENSE.md
//

namespace OneOfZero.Iptmerge.Netfilter
{
    public class NetfilterParser
    {
        private readonly StateParser _stateParser;

        public NetfilterParser(StateParser? stateParser = null)
        {
            _stateParser = stateParser ?? new StateParser();
        }

        public State ParseString(string state)
        {
            return _stateParser.Parse(state);
        }

        public State ParseFile(string path)
        {
            if (!File.Exists(path))
            {
                throw new ParseException($"File '{path}' does not exist");
            }

            return _stateParser.Parse(File.ReadAllText(path));
        }
    }
}
