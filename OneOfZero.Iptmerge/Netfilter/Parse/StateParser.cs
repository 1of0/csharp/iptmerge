using System.Collections.Generic;
using System.Text.RegularExpressions;
using OneOfZero.Iptmerge.Exceptions;
using OneOfZero.Iptmerge.Netfilter.Structure;

//
// SPDX-License-Identifier: MIT
// Find the full license text at: https://gitlab.com/1of0/csharp/iptmerge/-/blob/master/LICENSE.md
//

namespace OneOfZero.Iptmerge.Netfilter.Parse
{
    public class StateParser
    {
        private static readonly Regex PatternComment =
            new Regex(@"^\s*#", RegexOptions.Compiled);

        private static readonly Regex PatternTable =
            new Regex(@"^\*(.*)", RegexOptions.Compiled);

        private static readonly Regex PatternChain =
            new Regex(@"^:(.*?)\s+(.*?)\s+(\[\d+:\d+\])", RegexOptions.Compiled);

        private static readonly Regex PatternCommit =
            new Regex(@"^\s*COMMIT\s*$", RegexOptions.Compiled);

        private readonly RuleParser _ruleParser;

        public StateParser(RuleParser? ruleParser = null)
        {
            _ruleParser = ruleParser ?? new RuleParser();
        }

        public State Parse(string content)
        {
            var tables = new List<Table>();
            var tableChains = new List<Chain>();
            var tableRules = new List<Rule>();

            string? currentTableName = null;

            foreach (var line in content.Split("\n"))
            {
                if (string.IsNullOrWhiteSpace(line) || PatternComment.IsMatch(line))
                {
                    continue;
                }

                var tableMatch = PatternTable.Match(line);
                if (tableMatch.Success)
                {
                    currentTableName = tableMatch.Groups[1].Value;
                    continue;
                }

                if (currentTableName == null)
                {
                    throw new InvalidDefinitionException("Found definition outside table scope");
                }

                var chainMatch = PatternChain.Match(line);
                if (chainMatch.Success)
                {
                    tableChains.Add(new Chain(
                        chainMatch.Groups[1].Value,
                        chainMatch.Groups[2].Value,
                        chainMatch.Groups[3].Value
                    ));
                    continue;
                }

                if (PatternCommit.IsMatch(line))
                {
                    tables.Add(new Table(currentTableName, tableChains, tableRules));
                    currentTableName = null;
                    tableChains.Clear();
                    tableRules.Clear();
                    continue;
                }

                tableRules.AddRange(_ruleParser.Parse(line));
            }

            return new State(tables);
        }
    }
}
