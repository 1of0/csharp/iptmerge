using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;

//
// SPDX-License-Identifier: MIT
// Find the full license text at: https://gitlab.com/1of0/csharp/iptmerge/-/blob/master/LICENSE.md
//

namespace OneOfZero.Iptmerge.Netfilter.Parse
{
    public static class ParseHelper
    {
        public static readonly Dictionary<string, string> EscapeMap = new Dictionary<string, string>
        {
            {"\\", "\\\\"},
            {"\"", "\\\""},
            {"'", "\\'"},
        };

        public static string ShellEscape(this string input)
        {
            input = EscapeMap.Aggregate(
                input,
                (currentString, escapePair) => currentString.Replace(escapePair.Key, escapePair.Value)
            );
            return $"'{input}'";
        }

        public static string ShellUnescape(this string input)
        {
            if (input.StartsWith('"') && input.EndsWith('"') ||
                input.StartsWith("'") && input.EndsWith("'"))
            {
                input = input.Substring(1, input.Length - 2);
            }

            return EscapeMap.Aggregate(
                input,
                (intermediate, escapePair) => intermediate.Replace(escapePair.Value, escapePair.Key)
            );
        }

        public static bool OnlyContains(this List<string> list, string item)
        {
            return list.Count == 1 && list[0] == item;
        }

        public static string Canonicalize(this string value)
        {
            return CanonicalizePossibleNetworkLocation(value);
        }

        private static string CanonicalizePossibleNetworkLocation(string value)
        {
            // Really ghetto method to canonicalize an IP or CIDR.
            // TODO: Won't work with subnet masks e.g. 127.0.0.1/255.0.0.0

            var possibleIp = value;
            var prefixLength = -1;

            if (possibleIp.Contains('/'))
            {
                var parts = possibleIp.Split('/');

                if (parts.Length != 2
                    || !int.TryParse(parts[1], out prefixLength)
                    || prefixLength < 0
                    || prefixLength > 128)
                {
                    return value;
                }

                possibleIp = parts[0];
            }

            if (!possibleIp.Contains('.') && !possibleIp.Contains(':'))
            {
                return value;
            }

            if (IPAddress.TryParse(possibleIp, out var ip))
            {
                if (prefixLength == -1)
                {
                    prefixLength = ip.AddressFamily == AddressFamily.InterNetwork ? 32 : 128;
                }

                return $"{ip}/{prefixLength}";
            }

            return value;
        }
    }
}
