using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using OneOfZero.Iptmerge.Exceptions;
using OneOfZero.Iptmerge.Netfilter.Structure;
using OneOfZero.Iptmerge.Util;

//
// SPDX-License-Identifier: MIT
// Find the full license text at: https://gitlab.com/1of0/csharp/iptmerge/-/blob/master/LICENSE.md
//

namespace OneOfZero.Iptmerge.Netfilter.Parse
{
    public class RuleParser
    {
        private static readonly Dictionary<string, IEnumerable<string>> OptionAliases =
            new Dictionary<string, IEnumerable<string>>
            {
                {"A", new[] {"append"}},
                {"I", new[] {"insert"}},
                {"D", new[] {"delete"}},
                {"N", new[] {"new-chain"}},
                {"X", new[] {"delete-chain"}},
                {"F", new[] {"flush"}},
                {"s", new[] {"src", "source"}},
                {"d", new[] {"dst", "destination"}},
            };

        private static readonly IEnumerable<string> ChainActionOptions = new[]
        {
            "A", "I", "D", "N", "X", "F",
        };

        private static readonly IEnumerable<string> ExpandingOptions = new[]
        {
            "s", "d",
        };

        private static readonly IEnumerable<string> NonRepeatableOptions =
            ExpandingOptions
                .Concat(new[] {"comment"});

        private static readonly IEnumerable<string> SingleValueOptions =
            ChainActionOptions
                .Concat(ExpandingOptions)
                .Concat(new[] {"comment"});

        private static readonly Regex PatternOptionName = new Regex(@"^--?(.+)$", RegexOptions.Compiled);

        public IEnumerable<Rule> Parse(string ruleDefinition)
        {
            var parsedOptions = new List<RuleOption>();

            foreach (var tokenGroup in ruleDefinition.Tokenize().GroupTokens())
            {
                IEnumerable<string> remainingTokenGroup = tokenGroup.ToList();
                var negated = false;

                if (remainingTokenGroup.First() == RuleTokenizer.NegationToken)
                {
                    negated = true;
                    remainingTokenGroup = remainingTokenGroup.Skip(1);
                }

                var optionMatch = PatternOptionName.Match(remainingTokenGroup.FirstOrDefault() ?? "");

                if (!optionMatch.Success)
                {
                    throw new UnexpectedTokenException(
                        "Could not determine option name for token group.\n\n" +
                        $"Rule: {ruleDefinition}\nToken group:\n{tokenGroup.Dump()}\n"
                    );
                }

                remainingTokenGroup = remainingTokenGroup.Skip(1);

                parsedOptions.Add(new RuleOption(
                    optionMatch.Groups[1].Value,
                    remainingTokenGroup,
                    negated
                ));
            }

            var options = new RuleOptionCollection(parsedOptions);

            ValidateOptions(ruleDefinition, options);

            var actionOption = options.GetOptions(ResolveAliases(ChainActionOptions)).First();

            var rule = new Rule(
                actionOption.Name,
                actionOption.Values[0],
                options.WithoutOption(actionOption).GetOptions()
            );

            return ExpandMultiValueOptions(rule);
        }

        private void ValidateOptions(string ruleDefinition, RuleOptionCollection options)
        {
            if (options.GetOptions(ResolveAliases(ChainActionOptions)).Count() != 1)
            {
                throw new InvalidActionException(
                    "Rule does not contain exactly one chain action (e.g. -I INPUT).\n\n" +
                    $"Rule: {ruleDefinition}\n"
                );
            }

            foreach (var nonRepeatableOption in NonRepeatableOptions)
            {
                if (options.GetOptions(ResolveAliases(nonRepeatableOption)).Count() > 1)
                {
                    throw new InvalidDefinitionException(
                        $"Option {nonRepeatableOption} may only be provided once for every definition.\n\n" +
                        $"Rule: {ruleDefinition}\n"
                    );
                }
            }

            foreach (var singleValueOption in options.GetOptions(ResolveAliases(SingleValueOptions)))
            {
                if (singleValueOption.Values.Count != 1)
                {
                    throw new InvalidDefinitionException(
                        $"Option {singleValueOption.Name} must have exactly one value.\n\n" +
                        $"Rule: {ruleDefinition}\n"
                    );
                }
            }
        }

        private IEnumerable<Rule> ExpandMultiValueOptions(Rule rule)
        {
            var expandedRules = new List<Rule>();

            foreach (var expandingOption in ExpandingOptions)
            {
                var option = rule.GetOptions(ResolveAliases(expandingOption)).FirstOrDefault();
                var value = option?.Values.ElementAtOrDefault(0);

                if (option == null || value == null || !value.Contains(','))
                {
                    continue;
                }

                expandedRules.AddRange(value
                    .Split(',')
                    .Select(
                        item => rule.WithReplacedOption(option.Name, new RuleOption(option.Name, item, option.Negated))
                    )
                );
            }

            if (expandedRules.Count == 0)
            {
                expandedRules.Add(rule);
            }

            return expandedRules;
        }

        private IEnumerable<string> ResolveAliases(string optionName)
        {
            return ResolveAliases(new[] {optionName});
        }

        private IEnumerable<string> ResolveAliases(IEnumerable<string> optionNames)
        {
            var resolved = optionNames.ToList();

            foreach (var optionName in resolved.ToArray())
            {
                if (OptionAliases.TryGetValue(optionName, out var aliases))
                {
                    resolved.AddRange(aliases);
                }
            }

            return resolved;
        }
    }
}
