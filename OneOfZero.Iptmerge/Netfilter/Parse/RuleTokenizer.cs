using System.Collections.Generic;
using System.Text.RegularExpressions;
using OneOfZero.Iptmerge.Exceptions;

//
// SPDX-License-Identifier: MIT
// Find the full license text at: https://gitlab.com/1of0/csharp/iptmerge/-/blob/master/LICENSE.md
//

namespace OneOfZero.Iptmerge.Netfilter.Parse
{
    public static class RuleTokenizer
    {
        public const string NegationToken = "!";

        private static readonly Regex PatternAnyWhitespace =
            new Regex(@"^(\s+)", RegexOptions.Compiled);

        private static readonly Regex PatternQuotedStringStart =
            new Regex(@"^([""']).*", RegexOptions.Compiled);

        private static readonly Regex PatternShortOption =
            new Regex(@"^(-[^-""'\s])", RegexOptions.Compiled);

        private static readonly Regex PatternLongOptionWithAssignment =
            new Regex(@"^(--[^""'\s]+?=)", RegexOptions.Compiled);

        private static readonly Regex PatternAnythingUntilDelimiter =
            new Regex(@"^(.+?)(?:[""'\s]|$)", RegexOptions.Compiled);

        public static List<string> Tokenize(this string definition)
        {
            var position = 0;
            var tokens = new List<string>();

            while (position < definition.Length)
            {
                var chunk = definition.Substring(position);

                string? token;

                if (!string.IsNullOrEmpty(token = FindPattern(PatternAnyWhitespace, chunk)))
                {
                    position += token.Length;
                    continue;
                }

                if (!string.IsNullOrEmpty(token = FindQuotedString(chunk)))
                {
                    tokens.Add(token.ShellUnescape());
                    position += token.Length;
                    continue;
                }

                if (!string.IsNullOrEmpty(token = FindPattern(PatternLongOptionWithAssignment, chunk)))
                {
                    tokens.Add(token.TrimEnd('='));
                    position += token.Length;
                    continue;
                }

                token =
                    FindPattern(PatternShortOption, chunk) ??
                    FindPattern(PatternAnythingUntilDelimiter, chunk);

                // PatternAnythingUntilDelimiter should always match something unless the string is empty, in which
                // case this path shouldn't be reached.
                tokens.Add(token!);
                position += token!.Length;
            }

            return tokens;
        }

        public static List<List<string>> GroupTokens(this IEnumerable<string> tokens)
        {
            var groups = new List<List<string>>();
            var currentGroup = new List<string>();

            foreach (var token in tokens)
            {
                if (token == NegationToken || token.StartsWith('-') && !currentGroup.OnlyContains(NegationToken))
                {
                    if (currentGroup.Count > 0)
                    {
                        groups.Add(currentGroup);
                    }
                    currentGroup = new List<string>();
                }

                currentGroup.Add(token);
            }

            if (currentGroup.Count > 0)
            {
                groups.Add(currentGroup);
            }

            return groups;
        }

        private static string? FindPattern(Regex pattern, string chunk)
        {
            var match = pattern.Match(chunk);
            return match.Success ? match.Groups[1].Value : null;
        }

        private static string? FindQuotedString(string chunk)
        {
            var match = PatternQuotedStringStart.Match(chunk);

            if (!match.Success)
            {
                return null;
            }

            var quoteChar = match.Groups[1].Value[0];

            var isEscaping = false;

            for (var i = 1; i < chunk.Length; i++)
            {
                if (isEscaping)
                {
                    isEscaping = false;
                    continue;
                }

                if (chunk[i] == '\\')
                {
                    isEscaping = true;
                    continue;
                }

                if (chunk[i] == quoteChar)
                {
                    return chunk.Substring(0, i + 1);
                }
            }

            throw new UnclosedStringException($"Could not find closing quote for string in chunk: {chunk}");
        }
    }
}
