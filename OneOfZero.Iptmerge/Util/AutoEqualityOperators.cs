using System;

//
// SPDX-License-Identifier: MIT
// Find the full license text at: https://gitlab.com/1of0/csharp/iptmerge/-/blob/master/LICENSE.md
//

namespace OneOfZero.Iptmerge.Util
{
    public abstract class AutoEqualityOperators<T> : IEquatable<T> where T : class
    {
        public abstract bool Equals(T? other);

        public abstract override int GetHashCode();

        public override bool Equals(object? obj)
        {
            return Equals(obj as T);
        }

        public static bool operator ==(AutoEqualityOperators<T>? a, AutoEqualityOperators<T>? b)
        {
            if (ReferenceEquals(a, b))
            {
                return true;
            }

            if (ReferenceEquals(a, null) || ReferenceEquals(b, null))
            {
                return false;
            }

            return a.Equals(b);
        }

        public static bool operator !=(AutoEqualityOperators<T>? a, AutoEqualityOperators<T>? b)
        {
            return !(a == b);
        }
    }
}
