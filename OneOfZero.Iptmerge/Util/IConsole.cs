//
// SPDX-License-Identifier: MIT
// Find the full license text at: https://gitlab.com/1of0/csharp/iptmerge/-/blob/master/LICENSE.md
//

namespace OneOfZero.Iptmerge.Util
{
    public interface IConsole
    {
        public string ReadStandardInputAsString();

        public void WriteStandardOutput(string buffer);
    }
}
