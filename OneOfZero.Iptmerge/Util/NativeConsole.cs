using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;

//
// SPDX-License-Identifier: MIT
// Find the full license text at: https://gitlab.com/1of0/csharp/iptmerge/-/blob/master/LICENSE.md
//

namespace OneOfZero.Iptmerge.Util
{
    [ExcludeFromCodeCoverage]
    public class NativeConsole : IConsole
    {
        public string ReadStandardInputAsString()
        {
            using var reader = new StreamReader(Console.OpenStandardInput());
            return reader.ReadToEnd();
        }

        public void WriteStandardOutput(string buffer)
        {
            using var writer = new StreamWriter(Console.OpenStandardOutput());
            writer.Write(buffer);
        }
    }
}
