using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

//
// SPDX-License-Identifier: MIT
// Find the full license text at: https://gitlab.com/1of0/csharp/iptmerge/-/blob/master/LICENSE.md
//

namespace OneOfZero.Iptmerge.Util
{
    [ExcludeFromCodeCoverage]
    public static class DebugHelper
    {
        public static string Dump(this object subject)
        {
            return $"{subject}\n";
        }

        public static string Dump(this IEnumerable subject, int level = 1)
        {
            if (subject == null) return "*NULL*\n";

            var buffer = new StringBuilder();

            foreach (var item in subject)
            {
                buffer.Append("".PadLeft(level * 4));

                if (item == null)
                {
                    buffer.AppendLine("*NULL*,");
                }
                else if (!(item is string) && (item.GetType().IsArray || item is IEnumerable))
                {
                    var enumerableItem = (IEnumerable) item;
                    buffer.AppendLine(enumerableItem.Dump(level + 1));
                }
                else if (item.GetType().Name == typeof(KeyValuePair<object, object>).Name)
                {
                    var kvTypes = item.GetType().GetGenericArguments();

                    var key = item.GetType().GetProperty("Key")?.GetValue(item);
                    var value = item.GetType().GetProperty("Value")?.GetValue(item);

                    var keyString = key != null ? $"K: {kvTypes[0]} \"{key}\"" : "K: *NULL*";
                    if (key != null && !(key is string) && (key.GetType().IsArray || key is IEnumerable))
                    {
                        keyString = $"K: {((IEnumerable) key).Dump(level + 2)}";
                    }

                    var valueString = value != null ? $"V: {kvTypes[1]} \"{value}\"" : "V: *NULL*";
                    if (value != null && !(value is string) && (value.GetType().IsArray || value is IEnumerable))
                    {
                        valueString = $"V: {((IEnumerable) value).Dump(level + 2)}";
                    }

                    buffer.AppendLine($"{item.GetType()} {{");
                    buffer.AppendLine("".PadLeft((level + 1) * 4) + keyString);
                    buffer.AppendLine("".PadLeft((level + 1) * 4) + valueString);
                    buffer.AppendLine("".PadLeft(level * 4) + "},");
                }
                else
                {
                    buffer.AppendLine($"{item.GetType()} \"{item}\",");
                }
            }

            buffer.Append("".PadLeft((level - 1) * 4));

            return $"{subject.GetType()} {{\n{buffer}}},";
        }
    }
}
